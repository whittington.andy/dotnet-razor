using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using dotnet_razor.Data;
using dotnet_razor.Models;

namespace dotnet_razor.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly dotnet_razor.Data.dotnet_razorMovieContext _context;

        public DetailsModel(dotnet_razor.Data.dotnet_razorMovieContext context)
        {
            _context = context;
        }

        public Movie Movie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie = await _context.Movie.FirstOrDefaultAsync(m => m.ID == id);

            if (Movie == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
