## Razor Tutorial -- ubuntu 18.04, emacs, firefox and the command line.

This is an attempt to create from scratch the Razor tutorial from [Microsoft](https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/?view=aspnetcore-3.1).

Created an empty git project from gitlab then pulled the project from the command line.

Created the README.md with a touch command then created the dotnet project in the local directory.

![Project Creation](Images/project.creation.png)

The project creation and run was successful. 

[Part 2](https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/model?view=aspnetcore-3.1&tabs=visual-studio-code)

Has the reader create a model. Something like:

```c#
using System;
using System.ComponentModel.DataAnnotations;

namespace RazorPagesMovie.Models
{
    public class Movie
    {
        public int ID { get; set; }
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }
    }
}
```

Then add some global dotnet tools:

```script
dotnet tool install --global dotnet-ef
dotnet tool install --global dotnet-aspnet-codegenerator
```

Then install some packages:

```script
dotnet add package Microsoft.EntityFrameworkCore.SQLite
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```

Next the reader creates a database context and adds a connection:

```c#
//RazorPagesMovieContext.cs
using Microsoft.EntityFrameworkCore;

namespace RazorPagesMovie.Data
{
    public class RazorPagesMovieContext : DbContext
    {
        public RazorPagesMovieContext (
            DbContextOptions<RazorPagesMovieContext> options)
            : base(options)
        {
        }

        public DbSet<RazorPagesMovie.Models.Movie> Movie { get; set; }
    }
}
```

```json
//appsettings.json
  {
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "MovieContext": "Data Source=MvcMovie.db"
  }
}
```
Next comes added the context in Startup.cs.

Then comes a code scaffolding command:

```shell
dotnet aspnet-codegenerator razorpage -m Movie -dc RazorPagesMovieContext -udl -outDir Pages/Movies --referenceScriptLibraries
```

I ran into this [issue](https://github.com/dotnet/Scaffolding/issues/1384) at this point.

I had to downgrade the tool to 3.1.0.

```shell
dotnet tool uninstall --global dotnet-aspnet-codegenerator
dotnet tool install --global dotnet-aspnet-codegenerator --version 3.1.0
```
The rest of the tutorial process worked correctly.

Here is database seeding and a working service:

![Database Seeding](Images/Database.Seeding.png)

Database migrations working from the command line:

![Database Migrations](Images/Database.Migrations.png)

I finished the tutorial and pushed the project back to gitlab.

Then pulled the project to a temp folder and built, ran it and checked validation:

![Model Validation](Images/Model.Validation.png)
