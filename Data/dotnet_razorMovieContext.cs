using Microsoft.EntityFrameworkCore;

namespace dotnet_razor.Data
{
    public class dotnet_razorMovieContext : DbContext
    {
        public dotnet_razorMovieContext (
            DbContextOptions<dotnet_razorMovieContext> options)
            : base(options)
        {
        }

        public DbSet<dotnet_razor.Models.Movie> Movie { get; set; }
    }
}
